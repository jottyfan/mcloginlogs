#!/usr/bin/env python3

import re

import PyRSS2Gen
import logging as LOG
import dtparser
from datetime import datetime
from traceback import format_exception_only

class Element:
    def __init__(self, msg, datetime_str):
        self.msg = msg
        self.datetime = datetime_str

    def uid(self):
        return self.datetime

    def __str__(self):
        return ": ".join([self.datetime, self.msg])


def german_to_english(txt):
    LOG.debug("translate %s", txt)
    txt = txt.replace("Mai", "May")
    txt = txt.replace("Okt", "Oct")
    txt = txt.replace("Dez", "Dec")
    LOG.debug("translated: %s", txt)
    return txt


def parse_logfile(filename, needles):
    found = []
    with open(filename) as f:
        for line in f:
            for needle in needles:
                needleregex = needle.strip()
                try:
                    match = re.compile(needleregex).match(line)
                    if match:
                        LOG.debug("found %s", line)
                        time_string = match.group('ts')
                        msg = match.group('msg')
                        element = Element(msg, german_to_english(time_string))
                        LOG.debug("keep %s", element)
                        found.append(element)
                except re.error:
                    LOG.debug("cannot use %s as regex", needleregex)
    return found


def create_rss_item(title, rsslink, description, uid_str=None, pubdate=None):
    utc_now = datetime.utcnow() if None in (uid_str, pubdate) else None
    return PyRSS2Gen.RSSItem(title=title,
                             link=rsslink,
                             description=description,
                             guid=PyRSS2Gen.Guid(uid_str or utc_now.strftime('%Y%m%d%H%M%S')),
                             pubDate=pubdate or utc_now)


def generate_rss(lines, rsslink):
    found_items = []
    for line in lines:
        try:
            item = create_rss_item(
                title=line.msg,
                rsslink=rsslink,
                description=line.msg + " on " + line.datetime,
                uid_str=line.uid(),
                pubdate=dtparser.parse(line.datetime)
            )
        except Exception as exc:
            error_msg = format_exception_only(type(exc), exc)[0].strip()
            LOG.error("Unable to parse '%s': %s", line, error_msg)
            item = create_rss_item(
                title="error",
                rsslink=rsslink,
                description="Unable to parse '%s': %s" % (line, error_msg)
            )

        try:
            item.to_xml("UTF-8")
        except AttributeError as exc:
            error_msg = format_exception_only(type(exc), exc)[0].strip()
            LOG.error("item.to_xml failed for for '%s': %s", line, exc)
            item = create_rss_item(
                title="error",
                rsslink=rsslink,
                description="RSS generation error for item '%s': %s" % (line, error_msg)
            )
        found_items.append(item)

    rss = PyRSS2Gen.RSS2(
        title="Jottys minecraft login log feed",
        link=rsslink,
        description="reports the login and logout operations on Jottys minecraft server",
        lastBuildDate=datetime.utcnow(),
        items=found_items)

    return rss.to_xml("UTF-8")
