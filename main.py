#!/usr/bin/env python3

from flask import Flask
from configparser import ConfigParser, NoOptionError, NoSectionError
import logoptions
import logging as LOG
import functions

app = Flask(__name__)


@app.route("/")
def welcome():
    return "Login required; append it on the url"


@app.route("/<credential>")
def hello_name(credential):
    ini = ConfigParser()
    found = ini.read("/etc/mcloginlog.ini")
    regexneedles = [r'^\s*\[([^\]]*)\].*?\]:\s*(.*?(?:joined|left) the game.*)$']
    rsslink = 'http://www.jottyfan.de/minecraft/mcloginlog'
    if not found:
        return """<html><body>
                  <h1>there is no ini file; missing /etc/mcloginlog.ini</h1>
                  <h3>please fill it up with:</h3>
                  <p>[config]<br>credential=???<br>infile=???<br></p>
                  </body></html>"""
    else:
        try:
            pwd = ini.get("config", "credential")
        except NoOptionError:
            return ("missing option credential in /etc/mcloginlog.ini, "
                    "please add one (use pwgen for example)")
        except NoSectionError:
            return ("missing section config in /etc/mcloginlog.ini, "
                    "please add one by that line: [config]")
        try:
            filename = ini.get("config", "infile")
        except NoOptionError:
            return ("missing option infile in /etc/mcloginlog.ini; this is the full log file "
                    "path, typically minecraft's latest.log")
        try:
            regexneedlesstring = ini.get("config", "needles")
            regexneedles = regexneedlesstring.split(',')
        except NoOptionError:
            LOG.warn("no needles defined in /etc/mcloginlog.ini; using default one")
        try:
            rsslink = ini.get("config", "rsslink")
        except NoOptionError:
            return ("missing option rsslink in /etc/mcloginlog.ini; using default one")

    if credential == pwd:
        entries = functions.parse_logfile(filename, regexneedles)
        rss = functions.generate_rss(entries, rsslink)
        return rss
    else:
        LOG.info("wrong login trial")
        return "wrong login"


if __name__ == "__main__":
    app.run()
