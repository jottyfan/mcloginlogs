# mcloginlogs

show login and logout on a minecraft server by parsing the log files

# run

call `python3.7 main.py` (best within a container like tmux)

# compatibility

known to run with python3.7 (debian buster) and python3.5 (debian stretch)
